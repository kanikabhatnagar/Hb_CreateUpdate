package com.client;

import com.entity.Employee;
import com.sessionmanager.SessionManager;
import org.hibernate.Session;

public class Client {

    public static void main(String [] args){
        Employee e = new Employee("Kanika", "Udaipur");

        e.setAddress("Bihar");
        Session session = SessionManager.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(e);
        session.getTransaction().commit();
        session.close();
        SessionManager.closeSessionFactory();;

    }
}
