package com.entity;

import javax.persistence.*;

@Entity
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="e_id", length = 10)
    private int eId;
    @Column(name="e_name", length = 35)
    private String name;
    @Column(name="e_address") //default varchar(255)
    private String address;

    public Employee(String name, String address){
        this.name = name;
        this.address = address;
    }

    public int geteId() {
        return eId;
    }

    public void seteId(int eId) {
        this.eId = eId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }



}
